import Navbar from './navbar'
import Footer from './footer'
import { TawkMessengerContextStore } from './tawk.context';

export default function Layout({ children }: any) {
  return (
    <>
      <TawkMessengerContextStore>
        <Navbar />
        <main>{children}</main>
        <Footer />
      </TawkMessengerContextStore>
    </>
  )
}
