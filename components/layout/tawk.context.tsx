import TawkMessengerReact from '@tawk.to/tawk-messenger-react';
import { createContext, useRef } from "react";

interface TawkMessengerContextInterface {
  maximize: () => void;
  addEvent: (event: string, meta: any) => void;
}

export const TawkMessengerContext = createContext<TawkMessengerContextInterface>({ maximize: () => {}, addEvent: () => {} });

export const TawkMessengerContextStore = ({ children }: any) => {
  const tawkMessengerRef = useRef<any>();

  const maximize = () => {
    tawkMessengerRef.current.maximize();
  }

  const addEvent = (event: string, meta: any) => {
    tawkMessengerRef.current.addEvent(event, meta, function (error: any) {
      console.log(error);
    })
  }

  return (
    <TawkMessengerContext.Provider value={{ maximize, addEvent }}>
      
      {children}

      <TawkMessengerReact
        propertyId="6406f0c331ebfa0fe7f11bb1"
        widgetId="1gqthovhk"
        ref={tawkMessengerRef} />

    </TawkMessengerContext.Provider>
  )
}