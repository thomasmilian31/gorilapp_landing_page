interface Props {
  title: string;
  subtitle?: string;
  className?: string | undefined;
}

export default function Titles({ title, subtitle, className }: Props) {
  return (
    <div className={`flex gap-6 mx-4 md:mx-auto flex-col max-w-3xl ${className}`}>
      <h2 className='font-display text-white text-[28px] md:text-[40px] lg:text-[56px] text-center leading-really-tight title-shadow'>
        {title}
      </h2>

      {subtitle && <p className='font-body text-[#BDC4E3] text-center text-lg lg:text-[22px] whitespace-pre-wrap'>
        {subtitle}
      </p>}
    </div>
  )
}
