import Image from "next/image";

interface DescriptionProps {
  icon: string;
  title: string;
  description: string;
  className?: string | undefined;
}

export default function Description({ icon, title, description, className }: DescriptionProps) {
  return (
    <div className={`flex flex-col gap-4 ${className}`}>
      <CardIcon
        src={icon}
        alt='icon'
      />

      <h3 className="text-white font-body font-semibold text-xl md:text-2xl">
        {title}
      </h3>

      <p className="font-body text-[#BDC4E3] text-base">
        {description}
      </p>
    </div>
  )
}

interface CardIconProps {
  src: string;
  alt: string;
}

function CardIcon({ src, alt }: CardIconProps) {
  return (
    <div className="w-fit bg-[#0A1B5C] rounded-lg border border-[#1F3177] p-2">
      <Image src={src} alt={alt} width={24} height={24} />
    </div>
  )
}