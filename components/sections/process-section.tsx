import { useTranslation } from "next-i18next";
import Image from "next/image";
import Description from "../description";
import Titles from "../titles"

export default function ProcessSection() {
  const { t } = useTranslation('common')

  const descriptions = [
    {
      icon: "/images/material-symbols/checklist.svg",
      title: t("process.needs.title"),
      description: t("process.needs.description"),
    },
    {
      icon: "/images/material-symbols/brush.svg",
      title: t("process.design.title"),
      description: t("process.design.description"),
    },
    {
      icon: "/images/material-symbols/code.svg",
      title: t("process.development.title"),
      description: t("process.development.description"),
    },
    {
      icon: "/images/material-symbols/rocket_launch.svg",
      title: t("process.deploy.title"),
      description: t("process.deploy.description"),
    },
    {
      icon: "/images/material-symbols/temp_preferences_custom.svg",
      title: t("process.support.title"),
      description: t("process.support.description"),
    },
  ]

  return (
    <section id="steps" className="container xl:max-w-6xl mx-auto scroll-mt-32">
      <Titles
        title={t("process.title")}
        subtitle={t("process.subtitle")!} />

      <div className="flex mt-10 lg:mt-36">
        <div className="mx-auto">
          <div className="flex flex-col items-center w-[11px] lg:w-auto mx-4 sm:mx-10">
            <span className='w-[1px] h-16 bg-gradient-to-t from-white'></span>
          </div>
          {descriptions.map((description, index) => (
            <DescriptionLine
              key={index}
              icon={description.icon}
              title={description.title}
              description={description.description}
              end={index === descriptions.length - 1} />
          ))}
        </div>

        <ProcessImages />
      </div>

    </section>
  )
}

function ProcessImages() {
  return (
    <div className="absolute hidden lg:flex">
      <div className="w-[460px] h-[1280px] overflow-hidden rounded-[40px]">
        <div className="grid grid-cols-4 gap-6 grid-flow-row w-[1024px] -rotate-12 -translate-y-12 -translate-x-60">
          <div className="row-span-2 bg-[#576290] rounded-3xl"></div>
          <div className="h-[235px] bg-[#576290] rounded-3xl"></div>
          <div className="row-span-2 rounded-3xl relative overflow-hidden shadow-red-300/50 shadow-2xl bg-red-300">
            <Image src="/images/screens/home.svg" alt="wireframe home" fill={true} style={{ objectFit: 'cover', zIndex: 0 }} className="select-none" />
          </div>
          <div className="h-[235px] bg-[#576290] rounded-3xl"></div>


          <div className="row-span-2 h-[470px] rounded-3xl relative overflow-hidden shadow-yellow-300/50 shadow-2xl bg-yellow-300">
            <Image src="/images/screens/intro.svg" alt="Goril.app wireframe introduction" fill={true} style={{ objectFit: 'cover' }} className="select-none" />
          </div>
          <div className="row-span-2 h-[470px] bg-[#576290] rounded-3xl"></div>
          <div className="row-span-2 h-[470px] bg-[#576290] rounded-3xl"></div>
          <div className="row-span-2 h-[470px] rounded-3xl relative overflow-hidden shadow-green-300/50 shadow-2xl bg-gray-300">
            <Image src="/images/screens/pixels.svg" alt="wireframe pixels" fill={true} style={{ objectFit: 'cover' }} className="select-none" />
          </div>


          <div className="row-span-2 h-[470px] rounded-3xl relative overflow-hidden shadow-blue-300/50 shadow-2xl bg-blue-300">
            <Image src="/images/screens/share.svg" alt="wireframe share" fill={true} style={{ objectFit: 'cover' }} className="select-none" />
          </div>
          <div className="row-span-2 h-[470px] bg-[#576290] rounded-3xl"></div>
          <div className="row-span-2 h-[470px] bg-[#576290] rounded-3xl"></div>
          <div className="row-span-2 h-[470px] bg-[#576290] rounded-3xl"></div>

          <div className="row-span-2 h-[470px] bg-[#576290] rounded-3xl"></div>
          <div className="row-span-2 h-[470px] bg-[#576290] rounded-3xl"></div>
          <div className="h-[235px] bg-[#576290] rounded-3xl"></div>
          <div className="h-[235px] bg-[#576290] rounded-3xl"></div>
        </div>
      </div>
      <div className="w-72 -translate-x-72 bg-gradient-to-l from-rich-black"></div>
    </div>
  )
}

interface DescriptionLineProps {
  icon: string;
  title: string;
  description: string;
  end?: boolean | undefined;
  className?: string | undefined;
}

function DescriptionLine({ icon, title, description, end, className }: DescriptionLineProps) {
  return (
    <div className={`flex ${className}`}>
      <div className="w-full hidden lg:block"></div>

      <div className='flex flex-col items-center w-fit mx-4 sm:mx-10'>
        <span className='w-[11px] h-[11px] rounded-full border-2 my-1'></span>
        {!end && <span className='w-[1px] h-full border-r border-dashed'></span>}
        {end && <span className='w-[1px] h-40 bg-gradient-to-b from-white'></span>}
      </div>

      <Description
        icon={icon}
        title={title}
        description={description}
        className='max-w-md lg:max-w-none w-full mb-20 -translate-y-3 mr-4 sm:mr-0' />
    </div>
  )
}