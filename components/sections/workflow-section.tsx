import { useTranslation } from "next-i18next";
import Image from "next/image";
import DescriptionCard from "../description-card";
import Titles from "../titles";

export default function WorkflowSection() {
  const { t } = useTranslation('common')

  const cards = [
    {
      icon: "/images/material-symbols/visibility.svg",
      title: t("workflow.card1.title"),
      description: t("workflow.card1.description"),
    },
    {
      icon: "/images/material-symbols/rate_review.svg",
      title: t("workflow.card2.title"),
      description: t("workflow.card2.description"),
    },
    {
      icon: "/images/material-symbols/sync.svg",
      title: t("workflow.card3.title"),
      description: t("workflow.card3.description"),
    },
    {
      icon: "/images/material-symbols/smartphone.svg",
      title: t("workflow.card4.title"),
      description: t("workflow.card4.description"),
    },
  ]

  return (
    <div className="overflow-hidden">
      <section id="workflow" className="container xl:max-w-6xl mx-auto pt-4 scroll-mt-32">

        <Titles
          title={t("workflow.title")}
          subtitle={t("workflow.subtitle")!}
          className="mb-24" />

        <div className="relative md:h-[760px]">

          {/* Largest circle */}
          <div className="absolute m-auto inset-0 aspect-square w-full md:w-[640px] rounded-full border border-[#0E1638] p-10 flex items-center justify-center">
            {/* Circle rotating */}
            <div className="aspect-square w-full md:w-[520px] rounded-full border-dashed border-2 animate-spin-slow" />

            {/* Gradient */}
            <div className="absolute m-auto inset-0 w-full h-[330px] rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1] blur-3xl" />
          </div>

          {/* Milky way */}
          <div className="w-full h-full absolute m-auto inset-0">
            <Image src="/images/milky-way.svg" alt="milky way" fill={true} className="select-none animate-spin-slow" />
          </div>

          <div className="md:hidden grid grid-row-1 gap-4 mx-4">
            <DescriptionCard
              icon={cards[0].icon}
              title={cards[0].title}
              description={cards[0].description}
              className="bg-gradient-to-tl from-[#0B1A57]/50 border border-[#485895]/20" />

            <DescriptionCard
              icon={cards[1].icon}
              title={cards[1].title}
              description={cards[1].description}
              className="bg-gradient-to-tr from-[#0B1A57]/50 border border-[#485895]/20" />

            <DescriptionCard
              icon={cards[2].icon}
              title={cards[2].title}
              description={cards[2].description}
              className="bg-gradient-to-br from-[#0B1A57]/50 border border-[#485895]/20" />

            <DescriptionCard
              icon={cards[3].icon}
              title={cards[3].title}
              description={cards[3].description}
              className="bg-gradient-to-bl from-[#0B1A57]/50 border border-[#485895]/20" />
          </div>

          <div className="hidden md:block">
            <DescriptionCard
              icon={cards[0].icon}
              title={cards[0].title}
              description={cards[0].description}
              className="absolute top-0 left-40 w-1/3 bg-gradient-to-tl from-[#0B1A57]/50 border border-[#485895]/20" />

            <DescriptionCard
              icon={cards[1].icon}
              title={cards[1].title}
              description={cards[1].description}
              className="absolute top-40 right-0 w-1/3 bg-gradient-to-tr from-[#0B1A57]/50 border border-[#485895]/20" />

            <DescriptionCard
              icon={cards[2].icon}
              title={cards[2].title}
              description={cards[2].description}
              className="absolute bottom-0 right-40 w-1/3 bg-gradient-to-br from-[#0B1A57]/50 border border-[#485895]/20" />

            <DescriptionCard
              icon={cards[3].icon}
              title={cards[3].title}
              description={cards[3].description}
              className="absolute bottom-40 left-0 w-1/3 bg-gradient-to-bl from-[#0B1A57]/50 border border-[#485895]/20" />
          </div>

        </div>
      </section>
    </div>
  )
}

