import { useTranslation } from "next-i18next";
import DescriptionCard from "../description-card";
import Titles from "../titles";

export default function LeasingSection() {
  const { t } = useTranslation('common')

  const cards = [
    {
      icon: "/images/material-symbols/attach_money.svg",
      title: t("subscription.card1.title"),
      description: t("subscription.card1.description"),
    },
    {
      icon: "/images/material-symbols/self_improvement.svg",
      title: t("subscription.card2.title"),
      description: t("subscription.card2.description"),
    },
    {
      icon: "/images/material-symbols/filter_tilt_shift.svg",
      title: t("subscription.card3.title"),
      description: t("subscription.card3.description"),
    },
  ]

  return (
    <section id="subscription" className="container xl:max-w-6xl mx-auto scroll-mt-32">
      <Titles
        title={t("subscription.title")}
        subtitle={t("subscription.subtitle")!}
        className="mb-16"
      />

      {/* <div className="h-96 mt-16 rounded-3xl overflow-hidden relative">
        <Image src="/images/space.png" alt="space" fill={true} style={{objectFit: 'cover'}} className="select-none" />
      </div> */}

      {/* <p className="font-display text-3xl text-center max-w-2xl mx-auto my-16">{t("subscription.text")}</p> */}

      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6 relative mx-4 sm:mx-auto">
        {cards.map((card, index) => (
          <DescriptionCard
            key={index}
            icon={card.icon}
            title={card.title}
            description={card.description}
            className="bg-gradient-to-tl from-[#0B1A57]/60 to-[#0B1A57]/10 border border-[#485895]/50 relative z-10" />
        ))}

        <div className="absolute m-auto inset-0 blur-3xl rounded-full bg-gradient-to-b sm:bg-gradient-to-r from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1]" />
      </div>

    </section>
  )
}