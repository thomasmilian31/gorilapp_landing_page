import Image from "next/image";
import Titles from "../titles";
import styles from '../../styles/features.module.scss';
import { useTranslation } from "next-i18next";

export default function FeaturesSection() {
  const { t } = useTranslation('common')

  const cards = [
    [
      {
        icon: "/images/material-symbols/my_location.svg",
        title: t("features.geolocation"),
      },
      {
        icon: "/images/material-symbols/notifications.svg",
        title: t("features.notifications"),
      },
      {
        icon: "/images/material-symbols/credit_card.svg",
        title: t("features.payment"),
      },
      {
        icon: "/images/material-symbols/wifi_off.svg",
        title: t("features.offline"),
      },
      {
        icon: "/images/material-symbols/search.svg",
        title: t("features.search"),
      },
      {
        icon: "/images/material-symbols/mark_unread_chat_alt.svg",
        title: t("features.chat"),
      },
      {
        icon: "/images/material-symbols/sync.svg",
        title: t("features.sync"),
      },
    ],
    [
      {
        icon: "/images/material-symbols/account_circle.svg",
        title: t("features.connection"),
      },
      {
        icon: "/images/material-symbols/hotel_class.svg",
        title: t("features.rating"),
      },
      {
        icon: "/images/material-symbols/auto_graph.svg",
        title: t("features.analytics"),
      },
      {
        icon: "/images/material-symbols/qr_code.svg",
        title: t("features.qrcode"),
      },
      {
        icon: "/images/material-symbols/security.svg",
        title: t("features.security"),
      },
      {
        icon: "/images/material-symbols/pin_drop.svg",
        title: t("features.track"),
      },
      {
        icon: "/images/material-symbols/today.svg",
        title: t("features.calendar"),
      },
    ],
    [
      {
        icon: "/images/material-symbols/photo_camera.svg",
        title: t("features.camera"),
      },
      {
        icon: "/images/material-symbols/monetization_on.svg",
        title: t("features.monetize"),
      },
      {
        icon: "/images/material-symbols/language.svg",
        title: t("features.multilanguage"),
      },
      {
        icon: "/images/material-symbols/wallet.svg",
        title: t("features.wallet"),
      },
      {
        icon: "/images/material-symbols/dns.svg",
        title: t("features.api"),
      },
      {
        icon: "/images/material-symbols/dark_mode.svg",
        title: t("features.darkmode"),
      },
      {
        icon: "/images/material-symbols/brush.svg",
        title: t("features.theme"),
      },
    ]
  ]

  return (
    <section id="features" className="w-full md:container xl:max-w-6xl mx-auto overflow-x-hidden">

      <div className="w-full bg-[#1c1752] border-none md:border-solid border border-[#B1AAF4]/20 py-24 rounded-none md:rounded-[40px]">
        <Titles
          title={t('features.title')}
          subtitle={t('features.subtitle')!}
          className="mb-24" />

        <div className={`${styles.slider} flex flex-col gap-4 relative overflow-x-hidden`}>
          <ul className={`${styles.slideTrack1} relative z-10`}>
            {cards[0].map((item, index) => (
              <CardFeature
                key={index}
                title={item.title}
                icon={item.icon}
                className={styles.slide} />
            ))}
            {cards[0].map((item, index) => (
              <CardFeature
                key={index}
                title={item.title}
                icon={item.icon}
                className={styles.slide} />
            ))}
          </ul>
          <ul className={`${styles.slideTrack2} relative z-10`}>
            {cards[1].map((item, index) => (
              <CardFeature
                key={index}
                title={item.title}
                icon={item.icon}
                className={styles.slide} />
            ))}
            {cards[1].map((item, index) => (
              <CardFeature
                key={index}
                title={item.title}
                icon={item.icon}
                className={styles.slide} />
            ))}
          </ul>
          <ul className={`${styles.slideTrack3} relative z-10`}>
            {cards[2].map((item, index) => (
              <CardFeature
                key={index}
                title={item.title}
                icon={item.icon}
                className={styles.slide} />
            ))}
            {cards[2].map((item, index) => (
              <CardFeature
                key={index}
                title={item.title}
                icon={item.icon}
                className={styles.slide} />
            ))}
          </ul>

          <div className="absolute m-auto inset-0 blur-3xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1] opacity-80" />
        </div>
      </div>

    </section>
  )
}

interface CardFeatureProps {
  title: string;
  icon: string;
  className?: string | undefined;
}

function CardFeature({ title, icon, className }: CardFeatureProps) {
  return (
    <li className={`aspect-square border border-[#A49BF9]/50 rounded-xl p-8 flex flex-col items-center justify-between bg-gradient-to-tl from-[#01061A]/70 to-[#01061A]/10 backdrop-blur-xl ${className}`}>
      <div className="relative w-[58px] h-[58px]">
        <Image src={icon} alt="icon" height={58} width={58} className="opacity-90" />
      </div>
      <p className="font-body font-semibold text-white truncate opacity-90">{title}</p>
    </li>
  )
}
