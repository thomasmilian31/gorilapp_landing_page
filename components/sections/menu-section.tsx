import { useTranslation } from "next-i18next";
import Image from "next/image";
import Titles from "../titles";

export default function MenuSection() {
  const { t } = useTranslation('common')

  return (
    <section className="relative">
      <div className="relative z-10 container xl:max-w-6xl mx-auto">
        <Titles
          title={t("menu.title")}
          subtitle={t("menu.subtitle")!} />

        <div className="relative grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 sm:gap-6 mt-28 mx-4 sm:mx-0">

          <MenuCard
            title={t("menu.card1.title")}
            description={t("menu.card1.description")}
            icon="/images/menu-payments.svg"
            classNameIcon="mb-4"
            className="col-span-1 sm:col-span-2 bg-gradient-to-tl from-[#01061A] to-[#01061A]/10" />

          <MenuCard
            title={t("menu.card2.title")}
            description={t("menu.card2.description")}
            icon="/images/menu-bolt.svg"
            classNameIcon="mb-8"
            className="bg-gradient-to-tr from-[#01061A] to-[#01061A]/10" />

          <MenuCard
            title={t("menu.card3.title")}
            description={t("menu.card3.description")}
            icon="/images/menu-groups.svg"
            className="bg-gradient-to-bl from-[#01061A] to-[#01061A]/10 " />

          <MenuCard 
            title={t("menu.card4.title")}
            description={t("menu.card4.description")}
            icon="/images/menu-public.svg"
            classNameIcon="mb-1 sm:mb-8"
            className="col-span-1 sm:col-span-2 bg-gradient-to-br from-[#01061A] to-[#01061A]/10" />

          <div className="absolute -z-10 m-auto inset-0 blur-3xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1]" />

        </div>
      </div>

      <Image src='/images/milky-way.svg' alt='milky way' height={500} width={500} className="absolute mx-auto w-full -top-20 md:-top-96" />
    </section>
  )
}

interface MenuCardProps {
  className?: string;
  title: string;
  description: string;
  icon: string;
  classNameIcon?: string;
}

function MenuCard({ className, title, description, icon, classNameIcon }: MenuCardProps) {
  return (
    <div className={`h-96 rounded-2xl text-center border border-[#626B92]/40 flex flex-col justify-end pb-12 px-4 backdrop-blur-xl ${className}`}>
      <div className={`w-[200px] h-[200px] relative mx-auto ${classNameIcon}`}>
        <Image src={icon} alt='rocket' fill={true} style={{ mixBlendMode: "color-dodge" }} />
        <div className="absolute -z-10 m-auto inset-4 blur-2xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1] opacity-90" />
      </div>

      <h3 className="font-display text-[20px] md:text-[26px] leading-tight">{title}</h3>
      <p className="font-body text-base text-light-periwinkle leading-tight mt-4 whitespace-pre-wrap">{description}</p>
    </div>
  )
}
