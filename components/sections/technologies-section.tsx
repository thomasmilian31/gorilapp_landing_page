import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";

export default function TechnologiesSection() {
  const { t } = useTranslation('common')

  return (
    <div className="container xl:max-w-6xl mx-auto px-2 relative">
      <h2 className="relative z-10 font-display text-center text-[28px] title-shadow">{ t("technologies.title") }</h2>
      <div className="relative z-10  flex justify-center items-center gap-10 mt-10">
        {/* Flutter */}
        <Link href="https://flutter.dev" rel="noopener noreferrer" target="_blank">
          <Image src='/images/icon-flutter.png' alt="Flutter icon" height={70} width={70} />
        </Link>

        {/* Dart */}
        <Link href="https://dart.dev" rel="noopener noreferrer" target="_blank">
          <Image src='/images/icon-dart.png' alt="Dart icon" height={70} width={70} />
        </Link>

        {/* Python */}
        <Link href="https://www.python.org" rel="noopener noreferrer" target="_blank">
          <Image src='/images/icon-python.png' alt="Python icon" height={70} width={70} />
        </Link>

        {/* Firebase */}
        <Link href="https://firebase.google.com" rel="noopener noreferrer" target="_blank">
          <Image src='/images/icon-firebase.png' alt="Firebase icon" height={62} width={62} />
        </Link>

        {/* Fastlane */}
        <Link href="https://fastlane.tools" rel="noopener noreferrer" target="_blank">
          <Image src='/images/icon-fastlane.png' alt="Fastlane icon" height={70} width={70} />
        </Link>

        {/* Docker */}
        <Link href="https://www.docker.com" rel="noopener noreferrer" target="_blank">
          <Image src='/images/icon-docker.webp' alt="Docker icon" height={70} width={70} />
        </Link>
      </div>

      <div className="absolute m-auto inset-y-0 inset-x-0 lg:inset-x-32 blur-3xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1]" />
    </div>
  )
}
