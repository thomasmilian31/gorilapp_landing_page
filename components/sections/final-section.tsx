import { useTranslation } from "next-i18next";
import Image from "next/image";
import ContactUsButton from "../contact-us-button";

export default function FinalSection() {
  const { t } = useTranslation('common')

  return (
    <section className="container xl:max-w-6xl mx-auto my-40">
      <div className="">

        {/* Logo */}
        <div className="mx-auto bg-[#7266ED08] w-[280px] h-[280px] p-5 rounded-full relative">
          <div className="bg-[#7266ED1A] w-full h-full p-5 rounded-full">
            <div className="bg-[#7266ED40] w-full h-full p-5 rounded-full">
              <div className="bg-[#7266ED66] w-full h-full p-5 rounded-full flex items-center justify-center">
                <Image src={'/images/goril-app.svg'} alt="Logo" height={60} width={60} />
              </div>
            </div>
          </div>

          <div className="absolute m-auto inset-0 lg:-inset-40 blur-3xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1] opacity-100 lg:opacity-50" />
        </div>

        <h2 className="font-display text-[28px] md:text-[40px] lg:text-[56px] text-center leading-tight whitespace-pre-line">
          {t("final.title")}
        </h2>

        <ContactUsButton context="final" className="btn-primary btn rounded-full normal-case px-8 w-fit flex mx-auto mt-10">
          <Image src='/images/material-symbols/rocket_launch.svg' alt='rocket icon' width={24} height={24} className="mr-2" />
          {t("final.button")}
        </ContactUsButton>

      </div>
    </section>
  )
}
