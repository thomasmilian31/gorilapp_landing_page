import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useContext } from "react";
import ContactUsButton from "../contact-us-button";
import { TawkMessengerContext } from "../layout/tawk.context";

export default function HeaderSection() {
  const { t } = useTranslation('common')

  return (
    <header className="relative z-0 w-full overflow-hidden">
      <div className='container xl:max-w-6xl mx-auto relative translate-x-0 pt-44 pb-32 flex flex-col items-center'>

        {/* Title, subtext and CTA */}
        <div className='mb-40 relative'>
          <div className="">
            {/* Title */}
            <h1 className='font-display font-bold text-white text-center text-3xl md:text-5xl lg:text-6xl not-italic whitespace-pre-line'>
              { t('home.header.title') }
            </h1>

            {/* Subtext */}
            <p className='font-body text-white text-center text-lg lg:text-2xl mt-6 lg:mt-10 leading-snug max-w-[350px] md:max-w-[480px] lg:max-w-3xl mx-auto'>
            { t('home.header.subtitle') }
            </p>

            {/* CTA */}
            <ContactUsButton context="header" className='mt-12 w-fit mx-auto btn bg-[#090C1E] rounded-full font-body text-white px-8 py-3 flex shadow-md shadow-slate-900 normal-case'>
              <Image src='/images/material-symbols/rocket_launch.svg' alt='rocket icon' width={24} height={24} className="mr-2" />
              { t('home.header.button') }
            </ContactUsButton>
          </div>

          {/* Gradient */}
          <div className="absolute -z-10 inset-0 blur-2xl sm:blur-3xl rounded-full bg-gradient-to-b from-[#D1815F22] via-[#9670B7] to-[#7266ED]" />
        </div>

        {/* Main Image */}
        <div className="w-5/6 sm:w-full rounded-2xl border border-white overflow-hidden">
          <video autoPlay={true} muted={true} loop={true}>
            <source src="/videos/hero.mp4" type="video/mp4" />
            <source src="/videos/hero-tablet.mp4" type="video/mp4" media="all and (max-width: 850px)" />
            <source src="/videos/hero-mobile.mp4" type="video/mp4" media="all and (max-width: 480px)" />
          </video>
        </div>

        {/* Background Ellipse */}
        <div className="-z-20 absolute ellipse w-[4122px] h-[3203px] bottom-28 mx-auto bg-light-periwinkle">
          {/* Lines */}
          {/* <div className="container w-screen h-full xl:max-w-6xl absolute inset-0 flex justify-between">
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299] hidden sm:block'></span>
          </div> */}
        </div>

        {/* Ellipse border */}
        <div className="-z-30 absolute ellipse w-[4122px] h-[3203px] bottom-[110px] mx-auto bg-[#EDEBFF] opacity-90"></div>

        {/* Ellipse shadow */}
        <div className="-z-40 absolute bottom-20 mx-auto blur-2xl">
          <div className="ellipse w-[4122px] h-[3203px] bg-[#6055da]"></div>
        </div>

      </div>
    </header>
  )
}
