import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import ContactUsButton from "../contact-us-button";

export default function BetaNavBar() {
  const { t } = useTranslation('common')

  const menus = [
    { name: t("navbar.subscription"), route: "/#subscription" },
    { name: t("navbar.steps"), route: "/#steps" },
    { name: t("navbar.features"), route: "/#features" },
    { name: t("navbar.workflow"), route: "/#workflow" },
    { name: t("navbar.pricing"), route: "https://simulator.goril.app" },
  ]

  return (
    <nav className="fixed z-50 backdrop-blur-xl navbar font-body">
      <div className="container xl:max-w-6xl mx-auto flex justify-between items-center">
        <div className="flex items-center">
          {/* Dropdown */}
          <div className="dropdown">
            {/* Icon dropdown */}
            <label tabIndex={0} className="btn btn-ghost lg:hidden">
              <Image src={'/images/material-symbols/menu.svg'} alt="Menu icon" height={24} width={24} />
            </label>

            {/* Menu dropdown */}
            <ul tabIndex={0} className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-52 text-black">
              {menus.map((menu) => (
                <li key={menu.name}>
                  <Link href={menu.route}>{menu.name}</Link>
                </li>
              ))}
            </ul>
          </div>

          <Link href='/' className="btn btn-ghost normal-case text-xl">
            <Image src={'/images/goril-app.svg'} alt="Logo" height={24} width={24} className="mr-2" />
            {t("goril.app")}
          </Link>
        </div>
      </div>
    </nav >
  )
}
