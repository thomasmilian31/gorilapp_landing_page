import { useTranslation } from "next-i18next";
import Image from "next/image";
import JoinBetaButton from "./beta-button";
import DownloadButton from "./download-button";

export default function BetaHeaderSection() {
  const { t } = useTranslation('common')

  return (
    <header className="relative z-0 w-full overflow-hidden">
      <div className='container xl:max-w-6xl mx-auto relative translate-x-0 pt-32 pb-32 flex flex-col items-center'>
        {/* Same as */}
        {/* Title, subtext and CTA */}
        <div className='mb-20 relative'>
          <div className="grid place-items-center">
            {/* Title */}
            <h1 className='font-display font-bold text-white text-center text-3xl md:text-5xl lg:text-6xl not-italic whitespace-pre-line'>
              {/* {t('beta.header.title')} */}
              {t('beta.header.title')}
            </h1>

            {/* Subtext */}
            <p className='font-body text-white text-center text-lg lg:text-2xl mt-6 lg:mt-10 leading-snug max-w-[350px] md:max-w-[480px] lg:max-w-3xl mx-auto'>
              {t('beta.header.subtitle')}
            </p>

            {/* Subtext */}
            <p className='font-body text-white text-center text-lg lg:text-xl mt-4 lg:mt-6 leading-snug max-w-[350px] md:max-w-[480px] lg:max-w-3xl mx-auto'>
              {t('beta.header.subtitle2')}
            </p>

            {/* CTA */}
            <DownloadButton context="header" className="btn-primary btn rounded-full normal-case px-8 mt-14" platform="macos">
              <Image src='/images/apple.svg' alt='rocket icon' width={20} height={20} className="mr-3" />
              {t('beta.cta.download.macos')}
            </DownloadButton>
          </div>

          {/* Gradient */}
          <div className="absolute -z-10 inset-0 blur-2xl sm:blur-3xl rounded-full bg-gradient-to-b from-[#D1815F22] via-[#9670B7] to-[#7266ED]" />
        </div>

        {/* Main Image */}
        <div className="w-5/6 sm:w-full rounded-2xl border border-white overflow-hidden">
          <Image src="/images/gorilapp-studio.png" alt="icon" width={100} height={100} style={{ height: "100%", width: "100%" }} />
        </div>

        {/* Background Ellipse */}
        <div className="-z-20 absolute ellipse w-[4122px] h-[3203px] bottom-28 mx-auto bg-light-periwinkle">
          {/* Lines */}
          {/* <div className="container w-screen h-full xl:max-w-6xl absolute inset-0 flex justify-between">
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299]'></span>
            <span className='w-[1px] h-full bg-[#9E98E299] hidden sm:block'></span>
          </div> */}
        </div>

        {/* Ellipse border */}
        <div className="-z-30 absolute ellipse w-[4122px] h-[3203px] bottom-[110px] mx-auto bg-[#EDEBFF] opacity-90"></div>

        {/* Ellipse shadow */}
        <div className="-z-40 absolute bottom-20 mx-auto blur-2xl">
          <div className="ellipse w-[4122px] h-[3203px] bg-[#6055da]"></div>
        </div>

      </div>
    </header>
  )
}
