import { useContext } from "react";
import { logEventAnalytics } from "../../utils/analytics";
import { TawkMessengerContext } from "../layout/tawk.context";

interface DownloadButtonProps {
  className?: string;
  children: React.ReactNode;
  context: string;
  platform: "macos";
}

export default function DownloadButton({ className, children, context, platform }: DownloadButtonProps) {
  const tawkMessengerContext = useContext(TawkMessengerContext);

  const onClick = () => {
    tawkMessengerContext.maximize()
    tawkMessengerContext.addEvent(
      'download-beta',
      {
        context: context,
        description: `User downloaded beta ${platform}.`,
      }
    )
    logEventAnalytics('download-beta', {
      context: context,
    })
    if (platform == "macos") {
      window.open('https://storage.googleapis.com/gorilapp_assets/gorilapp.dmg?ignoreCache=1')
      window.focus();
    }
  }

  return (
    <button onClick={onClick} className={`${className}`}>
      {children}
    </button>
  )
}