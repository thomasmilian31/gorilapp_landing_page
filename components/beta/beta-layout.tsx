import { ToastContainer } from 'react-toastify';
import Footer from '../layout/footer'
import BetaNavBar from './beta-navbar';
import 'react-toastify/dist/ReactToastify.css';

export default function BetaLayout({ children }: any) {
  return (
    <>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastStyle={{ backgroundColor: "#090C1E", color: "#FFFFFF" }}
      />
      <BetaNavBar />
      <main>{children}</main>
      <Footer />
    </>
  )
}
