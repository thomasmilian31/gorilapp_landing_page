import { useTranslation } from "next-i18next";
import { logEventAnalytics } from "../../utils/analytics";
import Image from "next/image";
import { useForm } from "react-hook-form";
import { functions } from "../../utils/firebase";
import { httpsCallable } from "firebase/functions";
import { useEffect, useState } from "react";
import LoadingComponent from "../loading";
import { ToastContainer, toast } from "react-toastify";

interface JoinBetaButtonProps {
  className?: string;
  children: React.ReactNode;
  context: string;
}

export default function JoinBetaButton({ className, children, context }: JoinBetaButtonProps) {
  const { t } = useTranslation('common')

  const [loading, setLoading] = useState(false);

  useEffect(() => { }, [loading])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: Object) => {
    if (loading) return;
    setLoading(true);
    const call = httpsCallable(functions, 'addUserToBetaWaitlist');
    await call(data)
      .then((result) => {
        // Read result of the Cloud Function.
        /** @type {any} */
        const data = result.data;
        console.log(`Success sending ${data}`);
        toast("🔥 Welcome on board & stay tuned!", {});
      })
      .catch((error) => {
        // Getting the Error details.
        const code = error.code;
        const message = error.message;
        const details = error.details;
        console.log(`Error sending ${message} ${details}`);
        toast.error('Something went wrong, try again later!', {});
        // ...
      });
    setLoading(false);
    logEventAnalytics('join-beta', {
      context: context,
    });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={`flex w-full max-w-xl gap-x-4 ${className}`}>
      <label htmlFor="email-address" className="sr-only">Email address</label>
      <input id="email-address" type="email" autoComplete="email" className="min-w-0 flex-auto rounded-full bg-white bg-opacity-60 px-3.5 py-2 text-white shadow-sm ring-2 ring-white ring-opacity-30 focus:outline-none focus:ring-[#090C1E] focus:ring-opacity-30" placeholder="Enter your email"  {...register('email')} />
      {errors.title && (
        <div className="mb-3 text-normal text-red-500">
          Email is required.
        </div>
      )}
      <button type="submit" className="w-fit mx-auto my-auto btn bg-[#090C1E] rounded-full font-body text-white px-8 flex shadow-md shadow-slate-900 normal-case">
        {loading ? <LoadingComponent /> : ''}
        {loading ? '' : t('beta.cta.joinBeta')}
      </button>
    </form>
  )
}