import { useContext } from "react";
import { logEventAnalytics } from "../utils/analytics";
import { TawkMessengerContext } from "./layout/tawk.context";

interface ContactUsButtonProps {
  className?: string;
  children: React.ReactNode;
  context: string;
}

export default function ContactUsButton({ className, children, context }: ContactUsButtonProps) {
  const tawkMessengerContext = useContext(TawkMessengerContext);

  const analyticsLog = () => {
    logEventAnalytics('begin_generate_lead', {
      context: context,
    })
  }

  const analyticsGenerateLead = () => {
    logEventAnalytics('generate_lead', {
      context: context,
    })
  }

  const onClick = () => {
    tawkMessengerContext.maximize()
    tawkMessengerContext.addEvent(
      'contact-us',
      {
        context: context,
        description: 'User requested general information about our services.',
      }
    )

    logEventAnalytics('contact-us', {
      context: context,
    })
  }

  return (
    <button onClick={onClick} className={`${className}`}>
      {children}
    </button>
  )
}