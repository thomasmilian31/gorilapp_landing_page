import { useTranslation } from "next-i18next"

export default function PricingHeaderSection() {
  const { t } = useTranslation('common')

  return (
    <header className="relative z-0 w-full overflow-x-hidden pt-52">
      <div className="container mx-auto relative items-center flex flex-col">
        {/* Title */}
        <h1 className="font-display font-bold text-[40px] md:text-[66px] text-center leading-tight whitespace-pre-line">
          {t("pricing.header.title")}
        </h1>

        {/* Ellipse shadow */}
        <div className="-z-40 absolute bottom-20 mx-auto blur-3xl">
          <div className="ellipse w-[4122px] h-[3203px] bg-[#7266ED47]"></div>
        </div>
      </div>
    </header>
  )
}
