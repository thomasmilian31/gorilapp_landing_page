import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useContext, useState } from "react";
import { useCollapse } from "react-collapsed";
import { logEventAnalytics } from "../../utils/analytics";
import { TawkMessengerContext } from "../layout/tawk.context";

export default function PricingFaqSection() {
  const tawkMessengerContext = useContext(TawkMessengerContext);
  const { t } = useTranslation('common')

  const faqs: { question: string, answer: string }[] = [
    {
      question: t('pricing.faq.question1'),
      answer: t('pricing.faq.answer1')
    },
    {
      question: t('pricing.faq.question2'),
      answer: t('pricing.faq.answer2')
    },
    {
      question: t('pricing.faq.question3'),
      answer: t('pricing.faq.answer3')
    },
    {
      question: t('pricing.faq.question4'),
      answer: t('pricing.faq.answer4')
    },
    {
      question: t('pricing.faq.question5'),
      answer: t('pricing.faq.answer5')
    },
    {
      question: t('pricing.faq.question6'),
      answer: t('pricing.faq.answer6')
    },
    {
      question: t('pricing.faq.question7'),
      answer: t('pricing.faq.answer7')
    },
  ]

  const [expandedFaq, setExpandedFaq] = useState<number | null>(null)

  function onToggle(index: number) {
    if (expandedFaq === index) {
      setExpandedFaq(null)
    } else {
      setExpandedFaq(index)
      tawkMessengerContext.addEvent('faq-question', { question: faqs[index].question })
      logEventAnalytics('faq-question', { question: faqs[index].question })
    }
  }

  return (
    <section className="max-w-2xl mx-auto flex flex-col items-center">
      <h2 className="font-display font-bold text-[26px] md:text-[32px] text-white">
        {t('pricing.faq.title')}
      </h2>

      <div className="flex flex-col mt-10 mx-4">
        {faqs.map((faq, index) => (
          <FaqLine key={'faq-' + index} question={faq.question} answer={faq.answer} isExpanded={expandedFaq === index} onToggle={() => onToggle(index)} />
        ))}
      </div>
    </section>
  )
}

interface FaqLineProps {
  question: string;
  answer: string;
  isExpanded: boolean;
  onToggle: () => void;
}

function FaqLine({ question, answer, isExpanded, onToggle }: FaqLineProps) {
  const { getCollapseProps, getToggleProps } = useCollapse({ isExpanded })

  return (
    <div className="border-b border-[#1A254B] text-white font-body w-full py-6">
      <button {...getToggleProps({ onClick: () => onToggle() })} className="flex justify-between w-full">
        <span className="text-xl text-left font-semibold">{question}</span>
        <Image src="/images/material-symbols/keyboard_arrow_down.svg" alt="icon" height={20} width={20} className={"transition " + (isExpanded ? 'rotate-180' : 'rotate-0')} />
      </button>

      <div {...getCollapseProps()}>
        <p className="mt-4 text-[#d0d7ed]">{answer}</p>
      </div>
    </div>
  )
}
