import { TFunction } from "next-i18next";
import { CardPricingProps } from "../card-pricing";
import { Group } from "./pricing-table-section";

export function getPlans(t: TFunction): CardPricingProps[] {
  return [
    {
      title: t("pricing.cards.plan1.title"),
      description: t("pricing.cards.plan1.description"),
      pricePerYear: 490,
      pricePerMonth: 590,
      downPayment: 1500,
      highlighted: false,
      featuresTitle: t("pricing.cards.plan1.featuresTitle"),
      features: [
        {
          title: t("pricing.cards.plan1.feature1"),
          description: t("pricing.cards.plan1.feature1.description"),
        },
        {
          title: t("pricing.cards.plan1.feature2"),
          description: t("pricing.cards.plan1.feature2.description"),
        },
        {
          title: t("pricing.cards.plan1.feature3"),
          description: t("pricing.cards.plan1.feature3.description"),
        },
        {
          title: t("pricing.cards.plan1.feature4"),
          description: t("pricing.cards.plan1.feature4.description"),
        },
        {
          title: t("pricing.cards.plan1.feature5"),
          description: t("pricing.cards.plan1.feature5.description"),
        },
        {
          title: t("pricing.cards.plan1.feature6"),
          description: t("pricing.cards.plan1.feature6.description"),
        },
      ],
    },
  
    {
      title: t("pricing.cards.plan2.title"),
      description: t("pricing.cards.plan2.description"),
      pricePerYear: 990,
      pricePerMonth: 1190,
      downPayment: 3000,
      highlighted: false,
      featuresTitle: t("pricing.cards.plan2.featuresTitle"),
      features: [
        {
          title: t("pricing.cards.plan2.feature1"),
          description: t("pricing.cards.plan2.feature1.description"),
        },
        {
          title: t("pricing.cards.plan2.feature2"),
          description: t("pricing.cards.plan2.feature2.description"),
        },
        {
          title: t("pricing.cards.plan2.feature3"),
          description: t("pricing.cards.plan2.feature3.description"),
        },
        {
          title: t("pricing.cards.plan2.feature4"),
          description: t("pricing.cards.plan2.feature4.description"),
        }
      ]
    },
  
    {
      title: t("pricing.cards.plan3.title"),
      description: t("pricing.cards.plan3.description"),
      pricePerYear: 1990,
      pricePerMonth: 2390,
      downPayment: 6000,
      highlighted: true,
      featuresTitle: t("pricing.cards.plan3.featuresTitle"),
      features: [
        {
          title: t("pricing.cards.plan3.feature1"),
          description: t("pricing.cards.plan3.feature1.description"),
        },
        {
          title: t("pricing.cards.plan3.feature2"),
          description: t("pricing.cards.plan3.feature2.description"),
        },
        {
          title: t("pricing.cards.plan3.feature3"),
          description: t("pricing.cards.plan3.feature3.description"),
        },
        {
          title: t("pricing.cards.plan3.feature4"),
          description: t("pricing.cards.plan3.feature4.description"),
        },
        {
          title: t("pricing.cards.plan3.feature5"),
          description: t("pricing.cards.plan3.feature5.description"),
        },
        {
          title: t("pricing.cards.plan3.feature6"),
          description: t("pricing.cards.plan3.feature6.description"),
        },
        {
          title: t("pricing.cards.plan3.feature7"),
          description: t("pricing.cards.plan3.feature7.description"),
        }
      ]
    },
  
    {
      title: t("pricing.cards.plan4.title"),
      description: t("pricing.cards.plan4.description"),
      pricePerYear: 2990,
      pricePerMonth: 3590,
      downPayment: 9000,
      highlighted: false,
      featuresTitle: t("pricing.cards.plan4.featuresTitle"),
      features: [
        {
          title: t("pricing.cards.plan4.feature1"),
          description: t("pricing.cards.plan4.feature1.description"),
        },
        {
          title: t("pricing.cards.plan4.feature2"),
          description: t("pricing.cards.plan4.feature2.description"),
        },
        {
          title: t("pricing.cards.plan4.feature3"),
          description: t("pricing.cards.plan4.feature3.description"),
        },
        {
          title: t("pricing.cards.plan4.feature4"),
          description: t("pricing.cards.plan4.feature4.description"),
        },
        {
          title: t("pricing.cards.plan4.feature5"),
          description: t("pricing.cards.plan4.feature5.description"),
        }
      ]
    },
  
    {
      title: t("pricing.cards.plan5.title"),
      description: t("pricing.cards.plan5.description"),
      pricePerYear: 3990,
      pricePerMonth: 4790,
      downPayment: 12000,
      highlighted: true,
      featuresTitle: t("pricing.cards.plan5.featuresTitle"),
      features: [
        {
          title: t("pricing.cards.plan5.feature1"),
          description: t("pricing.cards.plan5.feature1.description"),
        },
        {
          title: t("pricing.cards.plan5.feature2"),
          description: t("pricing.cards.plan5.feature2.description"),
        },
        {
          title: t("pricing.cards.plan5.feature3"),
          description: t("pricing.cards.plan5.feature3.description"),
        },
        {
          title: t("pricing.cards.plan5.feature4"),
          description: t("pricing.cards.plan5.feature4.description"),
        },
        {
          title: t("pricing.cards.plan5.feature5"),
          description: t("pricing.cards.plan5.feature5.description"),
        },
        {
          title: t("pricing.cards.plan5.feature6"),
          description: t("pricing.cards.plan5.feature6.description"),
        }
      ]
    },
  
    {
      title: t("pricing.cards.plan6.title"),
      description: t("pricing.cards.plan6.description"),
      pricePerYear: 2990,
      pricePerMonth: 3590,
      downPayment: 9000,
      highlighted: false,
      featuresTitle: t("pricing.cards.plan6.featuresTitle"),
      customCard: true,
      features: []
    },
  ]
}

export function getGroups(t: TFunction): Group[] {
  return [
    // Application section
    {
      title: t("pricing.groups.application.title"),
      icon: "/images/material-symbols/code.svg",
      lines: [
        {
          title: t("pricing.groups.application.androidIos"),
          content: [
            { check: true },
            { check: true },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.application.storeDeploy"),
          content: [
            { check: true },
            { check: true },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.application.purchaseOption"),
          description: t("pricing.groups.application.purchaseOption.description")!,
          content: [
            { purchaseOption: true },
            { purchaseOption: true },
            { purchaseOption: true },
            { purchaseOption: true },
            { purchaseOption: true },
          ]
        },
        {
          title: t("pricing.groups.application.screens"),
          content: [
            { text: t("pricing.groups.application.screens.content", { number: 3 })! },
            { text: t("pricing.groups.application.screens.content", { number: 6 })! },
            { text: t("pricing.groups.application.screens.content", { number: 9 })! },
            { text: t("pricing.groups.application.screens.content", { number: 12 })! },
            { text: t("pricing.groups.application.screens.content", { number: 15 })! },
          ]
        },
        {
          title: t("pricing.groups.application.features"),
          content: [
            { text: t("pricing.groups.application.features.content.quarter", { number: 1 })! },
            { text: t("pricing.groups.application.features.content.quarter", { number: 1 })! },
            { text: t("pricing.groups.application.features.content.quarter", { number: 2 })! },
            { text: t("pricing.groups.application.features.content.month", { number: 1 })! },
            { text: t("pricing.groups.application.features.content.month", { number: 2 })! },
          ]
        },
        {
          title: t("pricing.groups.application.uxui"),
          content: [
            { text: t("pricing.groups.application.uxui.content")! },
            { text: t("pricing.groups.application.uxui.content")! },
            { text: t("pricing.groups.application.uxui.content")! },
            { text: t("pricing.groups.application.uxui.content")! },
            { text: t("pricing.groups.application.uxui.content")! },
          ]
        },
        {
          title: t("pricing.groups.application.languages"),
          content: [
            { text: '1' },
            { text: '2' },
            { text: '2' },
            { text: t("pricing.groups.application.languages.multi")! },
            { text: t("pricing.groups.application.languages.multi")! },
          ]
        },
        {
          title: t("pricing.groups.application.notifications"),
          content: [
            { text: '' },
            { text: t("pricing.groups.application.notifications.allUsers")! },
            { text: t("pricing.groups.application.notifications.allAndGroups")! },
            { text: t("pricing.groups.application.notifications.custom")! },
            { text: t("pricing.groups.application.notifications.custom")! },
          ]
        },
        {
          title: t("pricing.groups.application.tablet"),
          content: [
            { check: false },
            { check: false },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.application.landscape"),
          content: [
            { check: false },
            { check: false },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.application.geolocation"),
          content: [
            { text: '' },
            { text: '' },
            { text: t("pricing.groups.application.geolocation.static")! },
            { text: t("pricing.groups.application.geolocation.dynamic")! },
            { text: t("pricing.groups.application.geolocation.dynamic")! },
          ]
        },
        {
          title: t("pricing.groups.application.chat"),
          content: [
            { text: '' },
            { text: '' },
            { text: '' },
            { text: t("pricing.groups.application.chat.textOnly")! },
            { text: t("pricing.groups.application.chat.textAndMedia")! },
          ]
        },
        {
          title: t("pricing.groups.application.inAppPurchases"),
          content: [
            { check: false },
            { check: false },
            { check: false },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.application.offline"),
          content: [
            { check: false },
            { check: false },
            { check: false },
            { check: false },
            { check: true },
          ]
        },
      ]
    },

    // Integration section
    {
      title: t("pricing.groups.integrations.title"),
      icon: "/images/material-symbols/hotel_class.svg",
      lines: [
        {
          title: t("pricing.groups.integrations.auth"),
          content: [
            { text: '' },
            { text: '' },
            { text: t("pricing.groups.integrations.auth.email")! },
            { text: t("pricing.groups.integrations.auth.custom")! },
            { text: t("pricing.groups.integrations.auth.full")! },
          ]
        },
        {
          title: t("pricing.groups.integrations.analytics"),
          content: [
            { check: false },
            { check: false },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.integrations.backend"),
          content: [
            { check: false },
            { check: false },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.integrations.api"),
          content: [
            { check: false },
            { check: false },
            { check: false },
            { check: true },
            { check: true },
          ]
        },
      ]
    },

    // Support section
    {
      title: t("pricing.groups.support.title"),
      icon: "/images/material-symbols/smartphone.svg",
      lines: [
        {
          title: t("pricing.groups.support.maintenance"),
          content: [
            { check: true },
            { check: true },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.support.onlineSupport"),
          content: [
            { check: true },
            { check: true },
            { check: true },
            { check: true },
            { check: true },
          ]
        },
        {
          title: t("pricing.groups.support.timeSupport"),
          content: [
            { text: '4h' },
            { text: '8h' },
            { text: '10h' },
            { text: '15h' },
            { text: '20h' },
          ]
        },
      ]
    }
  ]
}