import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useState } from "react";
import { useCollapse } from "react-collapsed";
import CardPricing, { CardPricingProps } from "../card-pricing";
import { getGroups, getPlans } from "./pricing-cards-section-content";
import PricingTableSection, { Group, Line } from "./pricing-table-section";

export default function PricingCardsSection() {
  const { t } = useTranslation('common')
  const { getCollapseProps, getToggleProps, isExpanded } = useCollapse()

  const plans = getPlans(t)
  const groups = getGroups(t)

  // toggle pricing monthly/annually
  const [toggle, setToggle] = useState(true)

  const purchaseOptionCalendarLines: Line[] = []
  const variable = 4
  for (let i = 1; i <= 25; i++) {
    var title = t("pricing.purchaseOptionCalendar.month", { number: i })
    if (i === 25) title = t("pricing.purchaseOptionCalendar.25AndMore")

    purchaseOptionCalendarLines.push({
      title: title,
      content: plans.slice(0, -1).map((plan) => {
        const calc = ((plan.pricePerMonth * 24 - plan.downPayment) - plan.pricePerMonth * variable) * ((25 - i) / 24) + (plan.pricePerMonth * variable)
        return {
          text: `${calc.toFixed(0)} €`
        }
      })
    })
  }
  const purchaseOptionCalendarGroups: Group[] = [
    {
      title: t("pricing.purchaseOptionCalendar.title"),
      icon: "/images/material-symbols/hotel_class.svg",
      lines: purchaseOptionCalendarLines
    }
  ]


  // Add a state with the selected section
  const [sectionSelected, setSectionSelected] = useState(0)

  return (
    <div className="overflow-x-hidden">
      <div className="flex m-4 md:mb-0 md:translate-y-[35px]">
        <PricingSelector selected={sectionSelected} onClick={(index) => setSectionSelected(index)} />
      </div>

      <div className="mx-0 sm:mx-6 md:p-32 bg-[#040B27] rounded-[32px] border border-[#101A41]">

        {/* Toggle pricing */}
        <div className="flex justify-center mb-4 mt-4 md:mb-8 md:mt-0">
          <TogglePricing toggle={toggle} setToggle={setToggle} />
        </div>

        <section className="m-4 md:m-0 md:mx-auto max-w-7xl">
          {sectionSelected === 0
            // For small teams
            ? <div key={"A"} className="grid gap-4 grid-cols-1 lg:grid-cols-2 xl:grid-cols-3">
              <CardPricing displayAnnualPrice={toggle} {...plans[0]} />
              <CardPricing displayAnnualPrice={toggle} {...plans[1]} />
              <CardPricing displayAnnualPrice={toggle} {...plans[2]} className="col-span-1 lg:col-span-2 xl:col-span-1" />
            </div>

            // For businesses & entreprises
            : <div key={"B"} className="grid gap-4 grid-cols-1 lg:grid-cols-2 xl:grid-cols-3">
              <CardPricing displayAnnualPrice={toggle} {...plans[3]} />
              <CardPricing displayAnnualPrice={toggle} {...plans[4]} />
              <CardPricing displayAnnualPrice={toggle} {...plans[5]} />
            </div>
          }
        </section>
      </div>

      <div className="flex justify-center mt-16">
        <button {...getToggleProps()} className="btn btn-ghost flex normal-case font-medium text-sm text-white font-body">
          {t("pricing.cards.compare")}
          <Image src="/images/material-symbols/keyboard_arrow_down.svg" alt="icon" height={20} width={20} className={"ml-2 transition " + (isExpanded ? 'rotate-180' : 'rotate-0')} />
        </button>
      </div>

      <div {...getCollapseProps()} className="mt-8">
        <PricingTableSection plans={plans.slice(0, -1).map(e => e.title)} groups={groups} showContactSalesButton={true} tableId="main" />
      </div>

      {/* Modal Purchase Option Calendar */}
      <input type="checkbox" id="salut" className="modal-toggle" />
      <label htmlFor="salut" className="modal cursor-pointer">
        <label className="modal-box w-11/12 max-w-5xl bg-slate-800" htmlFor="">
          <PricingTableSection plans={plans.slice(0, -1).map(e => e.title)} groups={purchaseOptionCalendarGroups} showContactSalesButton={false} tableId="purchase-option" />
        </label>
      </label>
    </div>
  )
}

interface PricingSelectorProps {
  selected: number
  onClick: (index: number) => void
}

function PricingSelector({ selected, onClick }: PricingSelectorProps) {
  const { t } = useTranslation('common')

  return (
    <div className="flex gap-1 mx-auto bg-[#141C3C] border-4 border-[#0C122899] rounded-full font-body font-medium md:text-lg p-1">
      <button onClick={() => onClick(0)} className={"rounded-full h-full px-8 py-3 text-[#D2DBFF] border " + (selected === 0 ? 'bg-[#313D70] border-[#495588]' : 'bg-[#101732] border-[#101732]')}>
        {t("pricing.cards.selector.starter")}
      </button>

      <button onClick={() => onClick(1)} className={"rounded-full h-full px-8 py-3 border " + (selected === 1 ? 'bg-[#313D70] border-[#495588]' : 'bg-[#101732] border-[#101732]')}>
        <span className="linear">
          {t("pricing.cards.selector.business")}
        </span>
      </button>
    </div>
  )
}

function TogglePricing({ toggle, setToggle }: { toggle: boolean, setToggle: (value: boolean) => void }) {
  const { t } = useTranslation('common')

  return (
    <label className="label cursor-pointer">
      <span className={`label-text font-body mr-3 ${toggle ? '' : 'font-bold'}`}>{t("pricing.cards.toggle.monthly")}</span>
      <input type="checkbox" className="toggle toggle-primary" checked={toggle} onChange={() => setToggle(!toggle)} />
      <span className={`label-text font-body ml-3 ${toggle ? 'font-bold' : ''}`}>{t("pricing.cards.toggle.annually")}</span>
    </label>
  )
}
