import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useState } from "react";
import ContactSalesButton from "../contact-sales-button";

interface PricingTableProps {
  plans: string[];
  groups: Group[];
  showContactSalesButton: boolean;
  tableId: string;
}

interface Content {
  check?: boolean;
  text?: string;
  purchaseOption?: boolean;
}

export interface Line {
  title: string;
  description?: string;
  content: Content[];
}

export interface Group {
  title: string;
  icon: string;
  lines: Line[];
}

export default function PricingTableSection({ plans, groups, showContactSalesButton, tableId }: PricingTableProps) {
  // const tableId = "table" // Math.random().toString(36).substring(7);
  const [selectedPlan, setSelectedPlan] = useState(0)

  return (
    <div className="container mx-auto font-body font-normal text-sm text-[#d0d7ed] p-4">

      {/* Header */}
      <table className="w-full">
        <thead>

          {/* Header large screen */}
          <tr className="hidden lg:grid grid-cols-7">
            <th className="col-span-2"></th>
            {plans.map((plan, index) => {
              const id = `${tableId}-header-${index}`
              return (<TableHeader key={id} headerId={id} highlighted={false} title={plan} plans={plans} showCTA={showContactSalesButton} />)
            })}
          </tr>

          {/* Header small screen */}
          <tr className="lg:hidden">
            <TableHeader headerId={`${tableId}-header-small`} highlighted={false} title={plans[selectedPlan]} className="pb-10" switchPlan={true} plans={plans} onSwitchPlan={(index) => setSelectedPlan(index)} showCTA={showContactSalesButton} />
          </tr>

        </thead>
      </table>

      {/* Group */}
      <div className="flex flex-col gap-10">
        {groups.map((group, groupIndex) => (
          <table key={`${tableId}-group-${groupIndex}`} className="w-full">
            <tbody>

              {/* Group title */}
              <tr>
                <GroupTitle group={group} />

                {/* {group.lines[0].content.map((content, index) => (
                  <td key={`${tableId}-group-${groupIndex}-line-${index}`} className="py-4 border-b border-[#1A254B]" />
                ))} */}
              </tr>

              {/* Lines of the group large screen */}
              {group.lines.map((line, lineIndex) => (
                <tr key={`${tableId}-group-${groupIndex}-line-${lineIndex}`} className="grid grid-cols-3 lg:grid-cols-7">
                  {/* Title of line */}
                  <LineTitle line={line} />

                  {/* Content small */}
                  <Cell content={line.content[selectedPlan]} className="lg:hidden" />

                  {/* Content large */}
                  {line.content.map((content, contentIndex) => (
                    <Cell key={`${tableId}-group-${groupIndex}-line-${lineIndex}-large-content-${contentIndex}`} content={content} className="hidden lg:block" />
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        ))}
      </div>

    </div>
  )
}

interface TableHeaderProps {
  highlighted: boolean;
  title: string;
  switchPlan?: boolean;
  plans: string[];
  showCTA: boolean;
  className?: string;
  headerId: string;
  onSwitchPlan?: (index: number) => void;
}

function TableHeader({ highlighted, title, switchPlan, plans, showCTA, className, headerId, onSwitchPlan }: TableHeaderProps) {
  const key = headerId //Math.random().toString(36).substring(7);

  return (
    <th className={className + " text-left font-body font-semibold text-2xl p-2"}>
      <div className="flex w-full justify-between items-center">
        {title}
        {switchPlan &&
          <label htmlFor={key} className="text-sm font-normal text-[#d0d7ed] flex gap-2">
            Switch Plan
            <Image src="/images/material-symbols/checklist.svg" alt="icon" height={20} width={20} />
          </label>}
      </div>

      {/* CTA */}
      {showCTA && <ContactSalesButton highlighted={highlighted} context={{ plan: title, location: "table" }} className="mt-3" />}

      {/* Modal */}
      <input type="checkbox" id={key} className="modal-toggle" />
      <div className="modal">
        <div className="modal-box bg-slate-700">
          <h3 className="font-bold text-lg mb-6">Choose a plan</h3>

          <div className="flex flex-col gap-2 text-base">
            {plans.map((plan, index) => (
              <label key={`${headerId}-${index}`} htmlFor={key} onClick={() => onSwitchPlan?.(index)} className="btn normal-case">
                {plan}
              </label>
            ))}
          </div>
        </div>
      </div>
    </th>
  )
}

interface CellProps {
  content: Content;
  className?: string;
}

function Cell({ content, className }: CellProps) {
  const { t } = useTranslation('common')

  return (
    <td className={`${className} py-4 border-b border-[#1A254B]`}> {/* border-l */}
      <div className="flex justify-center">
        {content.check && <Image src="/images/material-symbols/check_circle.svg" alt="icon" height={20} width={20} />}
        {content.text && content.text}
        {content.purchaseOption &&
          <label htmlFor="salut" className="btn btn-ghost btn-xs text-sm font-normal normal-case flex font-body">
            <Image src="/images/material-symbols/check_circle.svg" alt="icon" height={20} width={20} className="mr-2" />
            {t("pricing.table.section.learnMore")}
          </label>
        }
      </div>
    </td>
  )
}

interface LineTitleProps {
  line: Line;
}

function LineTitle({ line }: LineTitleProps) {
  return (
    <th className="col-span-2 p-4 border-b border-[#1A254B] text-white text-left font-medium">
      <div className="flex gap-3">
        {line.title}
        {line.description &&
          <div className="tooltip" data-tip={line.description}>
            <Image src="/images/material-symbols/info.svg" alt="icon" height={20} width={20} />
          </div>}
      </div>
    </th>
  )
}

interface GroupTitleProps {
  group: Group;
}

function GroupTitle({ group }: GroupTitleProps) {
  return (
    <th className="col-span-2 space-x-2 border-b border-[#1A254B]">
      <div className="flex items-center text-white font-semibold text-2xl py-4">
        <div className="bg-primary rounded-md p-1 w-10 h-10 flex items-center justify-center mr-3">
          <Image src={group.icon} alt="icon" height={24} width={24} />
        </div>
        {group.title}
      </div>
    </th>
  )
}
