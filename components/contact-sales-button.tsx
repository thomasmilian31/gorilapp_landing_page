import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useContext } from "react";
import { logEventAnalytics } from "../utils/analytics";
import { TawkMessengerContext } from "./layout/tawk.context";

interface ContactSalesContext {
  plan: string;
  location: "card" | "table"; 
}

interface ContactSalesButtonProps {
  className?: string;
  highlighted: boolean;
  context: ContactSalesContext;
}

export default function ContactSalesButton({ className, highlighted, context }: ContactSalesButtonProps) {
  const tawkMessengerContext = useContext(TawkMessengerContext);
  const { t } = useTranslation('common')

  const onClick = () => {
    tawkMessengerContext.maximize()
    tawkMessengerContext.addEvent(
      'contact-sales',
      {
        description: `User requested information about our ${context.plan} subscription. He clicked on the ${context.location}.`,
      }
    )

    logEventAnalytics('contact-sales', {
      plan: context.plan,
      location: context.location,
    })
  }

  return (
    <button onClick={onClick} className={className + " btn btn-block flex justify-between normal-case font-medium text-sm text-white border " + (highlighted ? "bg-[#7266ED] border-[#B4ACFF]" : "bg-[#263262] border-[#55618F]")}>
      { t("contact.sales.button") }
      <Image src="/images/material-symbols/arrow_right_alt.svg" alt="icon" height={20} width={20} />
    </button>
  )
}
