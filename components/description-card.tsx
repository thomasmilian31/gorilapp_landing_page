import Description from "./description";

interface DescriptionCardProps {
  icon: string;
  title: string;
  description: string;
  className?: string | undefined;
}

export default function DescriptionCard({ icon, title, description, className }: DescriptionCardProps) {
  return (
    <div className={`backdrop-blur p-6 rounded-3xl ${className}`}>
      <Description icon={icon} title={title} description={description} />
    </div>
  )
}
