import { useTranslation } from "next-i18next";
import Image from "next/image";
import ContactSalesButton from "./contact-sales-button";

interface FeatureLine {
  title: string;
  description: string;
}

export interface CardPricingProps {
  title: string;
  description: string;
  pricePerYear: number;
  pricePerMonth: number;
  downPayment: number;
  featuresTitle: string;
  features: FeatureLine[];
  highlighted: boolean;
  customCard?: boolean;
  displayAnnualPrice?: boolean;
  className?: string;
}

export default function CardPricing({ title, description, pricePerYear, pricePerMonth, downPayment, featuresTitle, features, highlighted, customCard, displayAnnualPrice, className }: CardPricingProps) {
  const { t } = useTranslation('common')

  return (
    <div className={"relative font-body " + className}>
      {/* Highlighted background */}
      {highlighted &&
        <div className="absolute inset-0 blur-3xl rounded-full bg-gradient-to-b from-[#D1815F00] via-[#9670B79E] to-[#7266EDA1]" />
      }

      {/* Card */}
      <div className={"relative h-full rounded-2xl p-8 sm:p-10 flex flex-col " + (highlighted ? "card-pricing-highlighted" : "card-pricing")}>

        {/* Title */}
        <p className="bg-gradient-to-br from-[#A098F3] to-[#574BD2] bg-clip-text text-transparent font-display text-2xl font-bold">{title}</p>
        {/* Description */}
        <p className="mt-4 text-sm">{description}</p>

        {/* -------------- */}
        <hr className="border-[#1A223F] my-6" />

        {/* Price */}
        {/* <p className={"text-[#A5B0D9] text-[10px] " + (customCard ? "invisible" : "")}>{t("pricing.cards.from")}</p> */}
        <p className="font-semibold text-[40px] my-1">
          {!customCard ? t("pricing.cards.price", { price: displayAnnualPrice ? pricePerYear : pricePerMonth }) : t("pricing.cards.priceCustom")}
        </p>
        <p className="text-[#A5B0D9] text-[10px]">
          {!customCard && t("pricing.cards.perMonthAndDownPayment", { price: downPayment })}
          {customCard && t("pricing.cards.perMonthAndDownPaymentCustom")}
        </p>

        {/* -------------- */}
        <hr className="border-[#1A223F] my-6" />

        {/* Features */}
        <p className="font-semibold text-sm mb-4">{featuresTitle}</p>
        <div className="grid gap-4">
          {features.map((feature, index) => (
            <FeatureLine key={index} title={feature.title} description={feature.description} />
          ))}
        </div>

        {/* Space */}
        <div className="flex-auto h-20"></div>

        {/* Contact sales button */}
        <ContactSalesButton highlighted={highlighted} context={{ plan: title, location: "card" }} />

      </div>
    </div>
  )
}

function FeatureLine({ title, description }: FeatureLine) {
  return (
    <div className="flex">
      <Image src="/images/material-symbols/check_circle.svg" alt="icon" height={20} width={20} />
      <p className="text-sm font-normal ml-2">{title}</p>

      <div className="flex-grow" />

      <div className="tooltip tooltip-left md:tooltip-top shrink-0" data-tip={description}>
        <Image src="/images/material-symbols/info.svg" alt="icon" height={20} width={20} />
      </div>
    </div>
  )
}