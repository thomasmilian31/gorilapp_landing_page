import Head from "next/head";
import FeaturesSection from "../components/sections/features-section";
import FinalSection from "../components/sections/final-section";
import HeaderSection from "../components/sections/header-section";
import LeasingSection from "../components/sections/leasing-section";
import MenuSection from "../components/sections/menu-section";
import ProcessSection from "../components/sections/process-section";
import TechnologiesSection from "../components/sections/technologies-section";
import WorkflowSection from "../components/sections/workflow-section";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

export default function Home() {
  return (
    <div className="grid gap-44">

      <Head>
        <title>Goril.app - Launch your app faster, easier, safer</title>
        <meta name="description" content="Meet the new generation for modern mobile development. We build your app in weeks, enjoy it during years." key="desc" />
        <link rel="icon" href="/favicon.ico" />

        <meta property="og:site_name" content="Goril.app" />
        <meta property="og:title" content="Launch your app with Goril.app" />
        <meta
          property="og:description"
          content="Meet the new generation for modern mobile development. We build your app in weeks, enjoy it during years."
        />
        <meta
          property="og:image"
          content="/images/og-image.png"
        />
      </Head>

      <HeaderSection />
      <LeasingSection />
      <MenuSection />
      <ProcessSection />
      <TechnologiesSection />
      <FeaturesSection />
      <WorkflowSection />
      <FinalSection />
    </div>
  )
}

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'footer',
      ])),
      // Will be passed to the page component as props
    },
  }
}
