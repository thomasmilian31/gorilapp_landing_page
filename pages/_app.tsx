import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Layout from '../components/layout/layout'
import '../utils/firebase'
import { appWithTranslation } from 'next-i18next'
import BetaLayout from '../components/beta/beta-layout'

const App = ({ Component, pageProps, ...appProps }: AppProps) => {
  if ([`/beta`].includes(appProps.router.pathname)) {
    return (
      <BetaLayout>
        <Component {...pageProps} />
      </BetaLayout>
    )
  }
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}

export default appWithTranslation(App)
