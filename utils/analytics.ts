import { logEvent } from "firebase/analytics";
import { analytics } from "./firebase";

export function logEventAnalytics(eventName: string, eventParams?: { [key: string]: any; }) {
  if (analytics) {
    logEvent(analytics, eventName, eventParams);
  }
}