// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { Analytics, getAnalytics } from "firebase/analytics";
import { getFunctions } from 'firebase/functions';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB_nfUsPznbp9YsDUWfllov7cYra0bi0c4",
  authDomain: "gorilapp.firebaseapp.com",
  projectId: "gorilapp",
  storageBucket: "gorilapp.appspot.com",
  messagingSenderId: "754541334348",
  appId: "1:754541334348:web:ffacdf078bfb22d17889e9",
  measurementId: "G-F3KKSMSN5K"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Analytics
let analytics: Analytics | undefined;
if (typeof window !== "undefined") {
  analytics = getAnalytics(app);
}

const functions = getFunctions(app, 'europe-west1');

export { app, analytics, functions };