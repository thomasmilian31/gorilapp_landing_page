/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    container: {
      center: true,
    },
    fontFamily: {
      display: ['Sora', ...defaultTheme.fontFamily.serif],
      body: ['Outfit', ...defaultTheme.fontFamily.sans],
    },
    extend: {
      width: {
        '1/9': '11.11111111%',
      },
      colors: {
        'light-periwinkle': '#B7B3E3',
        'rich-black': '#01061A',
      },
      animation: {
        'spin-slow': 'spin 120s linear infinite',
      },
      lineHeight: {
        'really-tight': '1.15',
      },
    },
  },
  plugins: [require("daisyui")],
  daisyui: {
    // styled: true,
    themes: [
      {
        'gorilTheme': {
          primary: '#7266ED',
          neutral: '#252e52',
          'base-100': '#141C3C',
        }
      }
    ],
  }
}
